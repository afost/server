module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testTimeout: 130000,
}