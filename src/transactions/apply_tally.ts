import stringify from 'fast-stable-stringify'
import Shardus from 'shardus-global-server/src/shardus/shardus-types'
import create from '../accounts'
import _ from 'lodash'
import * as config from '../config'

export const validate_fields = (tx: Tx.ApplyTally, response: Shardus.IncomingTransactionResult) => {
  if (typeof tx.network !== 'string') {
    response.success = false
    response.reason = 'tx "network" field must be a string'
    throw new Error(response.reason)
  }
  if (tx.network !== config.networkAccount) {
    response.success = false
    response.reason = 'tx "network" field must be: ' + config.networkAccount
    throw new Error(response.reason)
  }
  if (_.isEmpty(tx.next) || typeof tx.next !== 'object') {
    response.success = false
    response.reason = 'tx "next" field must be a non empty object'
    throw new Error(response.reason)
  }
  if (_.isEmpty(tx.nextWindows) || typeof tx.nextWindows !== 'object') {
    response.success = false
    response.reason = 'tx "nextWindows" field must be a non empty object'
    throw new Error(response.reason)
  }
  if (typeof tx.next.title !== 'string') {
    response.success = false
    response.reason = 'tx "next parameter title" field must be a string.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.description !== 'string') {
    response.success = false
    response.reason = 'tx "next parameter description" field must be a string.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.nodeRewardInterval !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter nodeRewardInterval" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.nodeRewardAmount !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter nodeRewardAmount" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.nodePenalty !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter nodePenalty" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.transactionFee !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter transactionFee" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.stakeRequired !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter stakeRequired" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.maintenanceInterval !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter maintenanceInterval" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.maintenanceFee !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter maintenanceFee" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.proposalFee !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter proposalFee" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.devProposalFee !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter devProposalFee" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.faucetAmount !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter faucetAmount" field must be a number.'
    throw new Error(response.reason)
  }
  if (typeof tx.next.transactionFee !== 'number') {
    response.success = false
    response.reason = 'tx "next parameter defaultToll" field must be a number.'
    throw new Error(response.reason)
  }
  return response
}

export const validate = (tx: Tx.ApplyTally, wrappedStates: WrappedStates, response: Shardus.IncomingTransactionResult, dapp: Shardus) => {
  response.success = true
  response.reason = 'This transaction is valid!'
  return response
}

export const apply = (tx: Tx.ApplyTally, txId: string, wrappedStates: WrappedStates, dapp: Shardus) => {
  const network: NetworkAccount = wrappedStates[tx.network].data
  network.next = tx.next
  network.nextWindows = tx.nextWindows
  network.timestamp = tx.timestamp
  dapp.log(`APPLIED TALLY GLOBAL ${stringify(network)} ===`)
}

export const keys = (tx: Tx.ApplyTally, result: TransactionKeys) => {
  result.targetKeys = [tx.network]
  result.allKeys = [...result.sourceKeys, ...result.targetKeys]
  return result
}

export const createRelevantAccount = (dapp: Shardus, account: NodeAccount, accountId: string, tx: Tx.ApplyTally, accountCreated = false) => {
  if (!account) {
    account = create.nodeAccount(accountId)
    accountCreated = true
  }
  return dapp.createWrappedResponse(accountId, accountCreated, account.hash, account.timestamp, account)
}
